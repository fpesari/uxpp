# uxpp - Open URL in Xournal++ (via wkhtmltopdf)

uxpp is a very simple program which opens an URL in Xournal++.

It requires the external programs wkhtmltopdf and xournalpp in order
to work.

## Installation

```
gem build uxpp.gemspec
gem install uxpp*.gemspec --user
```

## Usage

`uxpp URL`

It is best used with a keyboard binding, like Meta + Shift + X - even better
if the clipboard is fed to it via `xclip -o`.

## License

Everything in this repository is released under the
GNU Affero General Public License, version 3 or any later version, unless
otherwise specified (and even in that case, it's all libre software).
