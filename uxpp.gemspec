# frozen_string_literal: true

# SPDX-License-Identifier: AGPL-3.0-or-later

Gem::Specification.new do |s|
  s.name          = 'uxpp'
  s.version       = '0.1.0'
  s.summary       = 'Open URL in Xournal++'
  s.description   = <<~DESC
    uxpp is a very simple program which opens an URL in Xournal++.

    It requires the external programs wkhtmltopdf and xournalpp in order
    to work.

    It is best used with a keyboard binding like Meta + Shift + X.
  DESC
  s.authors       = ['Fabio Pesari']
  s.homepage      = 'https://gitlab.com/fpesari/uxpp'
  s.files         = %w[.rubocop.yml uxpp.gemspec LICENSE bin/uxpp
                       README.md]
  s.executables   = %w[uxpp]
  s.license       = 'AGPL-3.0+'
  s.requirements << 'wkhtmltopdf'
  s.requirements << 'xournalpp'
end
